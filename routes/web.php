<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('/overview', function () {
    return view('overview');
})->name('overview');

Route::get('/milestone', function () {
    return view('milestone');
})->name('milestone');

Route::get('/boardofdirector', function () {
    return view('boardofdirector');
})->name('boardofdirector');

Route::get('/managementteam', function () {
    return view('managementteam');
})->name('managementteam');

Route::get('/corporategovernance', function () {
    return view('corporategovernance');
})->name('corporategovernance');


Route::get('/accreditations', function () {
    return view('accreditations');
})->name('accreditations');

Route::get('/groupstructure', function () {
    return view('groupstructure');
})->name('groupstructure');

Route::get('/upstream', function () {
    return view('upstream');
})->name('upstream');

Route::get('/downstream', function () {
    return view('downstream');
})->name('downstream');

Route::get('/investors', function () {
    return view('investors');
})->name('investors');

Route::get('/sustainability', function () {
    return view('sustainability');
})->name('sustainability');

Route::get('/newsroom', function () {
    return view('newsroom');
})->name('newsroom');


Route::get('/location', function () {
    return view('location');
})->name('location');

Route::get('/sitemap', function () {
    return view('sitemap');
})->name('sitemap');

Route::get('/privacypolicy', function () {
    return view('privacypolicy');
})->name('privacypolicy');


