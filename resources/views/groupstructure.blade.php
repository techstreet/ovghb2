@extends('layouts._partials.app')

@section('body')
		<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Who Are We / Group Structure</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->

							
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
				<div class="col_full">
						<div class="heading-block center">
							<h4>GROUP <span>STRUCTURE</span></h4>
						</div>	
				</div>
                <div class="section nomargin notoppadding"> 
					<div class="col_full center scrollable">
                        <div class="my-custom-scrollbar my-custom-scrollbar-primary">
						<img src="images/orgchart2.png" class="org-chart">
                        </div>
					</div>
                    
                   
                   
                    </div> <!--end div section-->
                    
                    
				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->
        
        
@stop

@section('modal')
@stop


@section('page_style')
<style>.my-custom-scrollbar {
  position: relative;
  width: 100%;
  height: 100%;
  overflow: auto;
}
</style> 
@stop

@section('page_script')
@stop

@section('init_script')
<script>
                    var myCustomScrollbar = document.querySelector('.my-custom-scrollbar');
var ps = new PerfectScrollbar(myCustomScrollbar);

var scrollbarY = myCustomScrollbar.querySelector('.ps.ps--active-y>.ps__scrollbar-y-rail');

myCustomScrollbar.onscroll = function() {
  scrollbarY.style.cssText = `top: ${this.scrollTop}px!important; height: 400px; right: ${-this.scrollLeft}px`;
}
                    </script>
@stop
