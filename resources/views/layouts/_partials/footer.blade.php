<!-- Footer
        ============================================= -->
        <footer id="footer" >
            

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Copyrights &copy; 2020 All Rights Reserved. <a href="#">Ocean Vantage Holdings Berhad</a> (1298917-H). 
                        <div class="copyright-links"><a href="{{route('sitemap')}}">Sitemap</a> / <a href="{{route('privacypolicy')}}">Privacy Policy</a></div>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="#" class="social-icon si-small si-colored si-rounded si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-colored si-rounded si-linkedin">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>
                        </div>

                        <div class="clear"></div>
                        <i class="icon-envelope2"></i> info@ovbhd.com <span class="middot">&middot;</span> <i class="icon-phone1"></i> +603-5886 2555
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->