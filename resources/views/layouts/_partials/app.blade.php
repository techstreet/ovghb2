@extends('layouts.master')

@section('content')
<body class="stretched no-transition">
  @include('layouts._partials.topbar')
  @yield('body') <!--content-->
  @include('layouts._partials.footer')
  @yield('modal')
</div>
	<!-- Go To Top ============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>
</body>
@stop