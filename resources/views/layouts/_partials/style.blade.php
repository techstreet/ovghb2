
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="styles/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="styles/style.css" type="text/css" />
	<link rel="stylesheet" href="styles/custom.css" type="text/css" />
	<link rel="stylesheet" href="styles/dark.css" type="text/css" />
	<link rel="stylesheet" href="styles/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="styles/animate.css" type="text/css" />
	<link rel="stylesheet" href="styles/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="styles/swiper.css" type="text/css" />

	<link rel="stylesheet" href="styles/responsive.css" type="text/css" />

	
	<link rel="stylesheet" href="styles/custom2.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/styles/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/styles/layers.css">
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/styles/navigation.css">


@yield('page_style')
