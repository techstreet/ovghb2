<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="OVG" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    @include('layouts._partials.style')
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Ocean Vantage Holdings Berhad</title>
</head>