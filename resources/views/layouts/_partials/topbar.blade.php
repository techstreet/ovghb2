
<div id="wrapper" class="clearfix">
<!-- Header ============================================= -->
        <header id="header" class="full-header" data-sticky-class="not-dark">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="{{route('index')}}" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png"></a>
                        <a href="{{route('index')}}" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul>
                            <li class=""><a href="{{route('index')}}"><div>Home</div></a></li>
                            <li class=""><a href="#"><div>Who We Are<i class="icon-angle-down"></i></div></a>
                                <ul>
                                    <li><a href="{{route('overview')}}"><div>Overview</div></a></li>
                                    <li><a href="{{route('milestone')}}"><div>Corporate Milestone</div></a></li>
                                    <li><a href="{{route('boardofdirector')}}"><div>Board of Director</div></a></li>
                                    <li><a href="{{route('managementteam')}}"><div>Management Team</div></a></li>
                                    <li><a href="{{route('corporategovernance')}}"><div>Corporate Governance</div></a></li>
                                    <li><a href="{{route('accreditations')}}"><div>Accreditations</div></a></li>
                                    <li><a href="{{route('groupstructure')}}"><div>Group Structure</div></a></li>
                                </ul>
                            </li>
                            <li class=""><a href="#"><div>What We Do<i class="icon-angle-down"></i></div></a>
                               <ul>
                                    <li><a href="{{route('upstream')}}"><div>Upstream</div></a></li>
                                    <li><a href="{{route('downstream')}}"><div>Downstream</div></a></li>
                                </ul>
                            </li>
                            <li class=""><a href="{{route('investors')}}"><div>Investors Relations</div></a></li>
                            <li class=""><a href="{{route('sustainability')}}"><div>Sustainability</div></a></li>
                            <li class=""><a href="{{route('newsroom')}}"><div>Newsroom</div></a></li>
                            <li class=""><a href="{{route('location')}}"><div>Location</div></a></li>
                        </ul>

                        
                        <!-- Top Search
                        ============================================= -->
                        <div id="top-search">
                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form action="search.html" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                            </form>
                        </div><!-- #top-search end -->

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop