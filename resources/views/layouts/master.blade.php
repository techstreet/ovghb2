<!DOCTYPE HTML>
<html lang="en">
 	@include('layouts._partials.head')	
    @yield('content')    
    @include('layouts._partials.script')
</html>