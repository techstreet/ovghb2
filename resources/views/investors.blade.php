@extends('layouts._partials.app')

@section('body')

		<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Investors</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
							
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
				  <div class="col_full">
							<div class="heading-block center">
								<h3>BURSA <span>ANNNOUNCEMENT</span></h3>
								 <a href="" class="button button button-rounded button-fill button-ovh button-small"><span>Read More</span></a>
								 
							</div>	
					</div>

				<div class="section nomargin "> 
					<div class="col_full">
						<div class="col_one_third">
							<ul>
								<li>Additional Listing Announcement</li>
								<li>Annual Audited Account</li>
								<li>Annual Report</li>
								<li>Change of Corporate Information</li>
								<li>Changes in Shareholdings</li>
								<li>Circular/Notice to Shareholders</li>
								<li>Entitlements</li>
								<li>Expiry / Maturity / Termination of Securities</li>
							</ul>
						</div>
						<div class="col_one_third">
							<ul>
							<li>Financial Results</li>
							<li>General Announcement</li>
							<li>General Meetings</li>
							<li>IPO Announcement/Admission to LEAP Market Announcement</li>
							<li>Important Relevant Dates for Renounceable Rights</li>
							<li>Investor Alert</li>
							<li>Listing Circulars</li>
							</ul>
						</div>
						<div class="col_one_third col_last">
						<ul>
							<li>Listing Information and Profile</li>
							<li>Prospectus</li>
							<li>Reply to Query</li>
							<li>Shares Buy Back</li>
							<li>Special Announcements</li>
							<li>Transfer of Listing</li>
							<li>Unusual Market Activity</li>
						</ul>
						</div>
					</div>

				</div>
					
				
					 

				</div>

				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->

        

@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
