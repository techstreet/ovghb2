@extends('layouts._partials.app')

@section('body')
	<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="topmargin-lg">
			</div>
		</section>
		
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Who Are We / Overview</h1>
			</div>
		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->				
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
				<div class="col_full">
						<div class="heading-block center">
							<h4>INCORPORATION & <span>HISTORY</span></h4>
						</div>
				</div>
					<div class="content-wrap container-overview">
						<img src="images/overview-img.jpg">
						<div class="row">
							<div class="col-md-4">
								<p><span>Our</span> Company was incorporated in Malaysia on 10 October 2018 under the Act as a private limited company. Our Company was subsequently converted to a public limited company on 10 April 2019. We were incorporated as a special purpose vehicle to facilitate the listing of our subsidiaries, OVE and OVIT on the ACE Market. Our Group is principally involved in the provision of support services for the O&G industry where we support both the upstream and downstream O&G activities.
								</p>
							</div>
							<div class="col-md-4">
								<p>Our Group’s history can be traced back to 2011, with the incorporation of OVE on 25 May 2011. OVE commenced operations as a provider of engineering, fabrication and NDT inspection and testing services, with Kenny Ronald Ngalin as Managing Director. Kenny Ronald Ngalin was responsible for building OVE’s business. 
								</p>
							</div>
							<div class="col-md-4">
								<p>Our first OVE office was set up in Miri, Sarawak in 2011, where we remain todate. O&G is a key economic sector in Sarawak, with the state’s history in O&G activities spanning back to 1910. Sarawak’s O&G industry contributed to 24.3% of Malaysia’s GDP in 2017. Being a company which has its origin in Sarawak, has positioned us to capitalize on the state’s O&G industry, given the scale of O&G activities in East Malaysia.
								</p>
							</div>
						</div>
						<div class="toggle text-center">
							<div class="togglet">// Read More</div>
							<div class="togglec">
								<div class="row">
									<div class="col-md-4">
										<p>We secured our first contract in May 2011 as a subcontractor for the provision of manpower and equipment to carry out engineering and fabrication works in Labuan. Then in 2012, we undertook our first overseas upstream oilfield services project where we were engaged to provide blasting and painting services as well as manpower and equipment supply in Saudi Arabia for an international drilling contractor, Transocean Asia Services Sdn Bhd.
										</p>
										<img src="images/overview-img3.jpg">
										<p>On 25 April 2012, OVIT was incorporated to provide NDT, material testing and welding services. With this, we further expanded our NDT inspection and testing services business to better cater for the growing needs of the O&G drilling contractors in Malaysia. 
										</p>
										<p>In July 2012, OVIT was recognised as an approved external specialist for hull gauging services by the American Bureau of Shipping (ABS), an international shipping classification organisation which provides classification services as well as on-the-ground technical services for the marine and offshore energy industries. The ABS recognition is commonly a requirement by our customers when offering hull gauging services, especially on ABS certified rigs / vessels. Furthermore, this recognition provides our customers the assurance of the quality and standard of our services. Further in the same year, OVE was registered with the CIDB as a Grade 3 contractor. 
										</p>
										<p>Leveraging on the experience of our Executive Director, Martin Philip King Ik Piau, who joined us in 2013 as General Manager, we developed our capabilities in design engineering and fabrication, as well as enhanced our project management delivery.
										</p>
										<p>In the same year, we set up a corporate office in Puchong, Selangor, to provide better support to our customers located in Peninsular Malaysia. OVE’s CIDB licence was also upgraded from Grade 3 to Grade 5 in 2013 and subsequently to Grade 7 in 2014, allowing us the flexibility to bid for civil, electrical and mechanical projects without cap on contract value.
										</p>
									</div>
									<div class="col-md-4">
										<p>In 2014, OVE obtained PETRONAS licence. We further expanded by opening a fabrication yard in Senai, Johor, as well as an office in Bintulu, Sarawak. In the same year, Yau Kah Tak joined us as Business Development Manager where he spearheaded our business development department. 
										</p>
										<p>In September 2014, OVE was appointed as the exclusive agent and partner for Noble, a United Kingdom based O&G offshore drilling contractor listed on the New York Stock Exchange. Leveraging on our relationship with Noble, we extended our services to include the provision of drillships, semi-submersibles and jack-up drilling rigs charter for upstream O&G activities in Malaysia. Through the Noble Arrangement, OVE secured a contract in May 2015 with an international oil company for the provision of a deepwater drillship, the Noble Bully II Drillship in Labuan. The contract includes the charter of the drillship, the manpower to operate the drillship including the machineries and equipment, for the international oil company to perform drilling operations. The commission income earned under this arrangement was RM0.56 million.
										</p>
										<img src="images/overview-img2.jpg">
										<p>The year 2015 saw OVE being certified by Det Norske Veritas (a certification body for the maritime, O&G and energy industries) for meeting its standards in the manufacture of offshore containers in relation to OVE’s fabrication works. Amongst the offshore containers that we fabricate are cargo baskets, lifting frames and gas bottle racks. </p>
										<p>In 2016, OVE was certified ISO 9001:2015 compliant by Bureau Veritas Certification (Malaysia) Sdn Bhd for EPC, installation, commissioning and maintenance services of onshore and offshore facilities for O&G, petrochemical and related industries. In the same year, OVIT was certified to conduct thickness measurement of hull structures by Bureau Veritas Certification (Malaysia) Sdn Bhd.</p>
										<p>Despite the challenging operating environment in the O&G industry due to the fluctuations in crude oil prices between 2016 and 2018, our business recorded a rise in revenue from RM17.65 million in FYE 2016 to RM30.49 million in FYE 2018.</p>
									</div>
									<div class="col-md-4">
										<p>In November 2018, both Martin Philip King Ik Piau and Yau Kah Tak (through his wife, Mary King Siaw Ning, who is also the sister of Martin Philip King Ik Piau) emerged as substantial shareholders in OVE alongside Kenny Ronald Ngalin, when they exercised the Call Options which were previously extended to them by Kenny Ronald Ngalin in 2015 and 2017 respectively. </p>
										<p>In January 2019, OVIT was approved to engage in the provision of thickness measurements on ships using standards requirements set by Nippon Kaiji Kyokai, a ship classification society based in Japan. This enables the Group to perform work on ships registered under the society.</p>
										<p>Our Puchong office was redesignated as our corporate office in 2013, housing our engineering, procurement, business development, finance and human resources departments, which were relocated to Puchong from our Miri office. We relocated some of our operations to Puchong as our major customer’s offices / headquarters are based in Klang Valley. Nonetheless, our office in Miri, headed by our General Manager – Thomas Jalong, manages our projects in East Malaysia. Further, our Miri office functions as our recruitment centre where we source offshore and onshore personnel for our supply of manpower services. Kenny Ronald Ngalin, Martin Philip King Ik Piau and Yau Kah Tak oversee the operations of all our offices, commuting regularly on a weekly basis between Peninsular Malaysia and East Malaysia for management visits to our offices, client meetings as well as site visits to our clients’ premises.</p>
										<img src="images/overview-img4.jpg">
										<p>Since the incorporation of OVE, we have grown with our business evolving from the mere supply of manpower and equipment to a wider range of services encompassing project management, EPC and drilling rig charter services. We have delivered our services for customers operating in over 15 countries in the regions of Asia Pacific, Middle East, Africa, Europe and America.</p>
									</div>
								</div>
							</div>
						</div>

						


					 </div>
				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->
@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop