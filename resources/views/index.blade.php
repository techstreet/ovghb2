@extends('layouts._partials.app2')

@section('body')

		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element slider-parallax swiper_wrapper full-screen clearfix  swiper_wrapper ">
			<div class="slider-parallax-inner">

				<div class="swiper-container swiper-parent">
					<div class="swiper-wrapper">
						<div class="swiper-slide dark" style="background-image: url('images/slider/slider1.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-center">
                                    <img src="images/slider/slider-title.png" class="bottommargin-sm" data-animate="fadeInUp" data-delay="0">
									
								</div>
							</div>
						</div>
                        <div class="swiper-slide dark" style="background-image: url('images/slider/slider2.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-center">
                                    <img src="images/slider/slider-title.png" class="bottommargin-sm" data-animate="fadeInUp" data-delay="0">
									
								</div>
							</div>
						</div>
						
						
                        
					
					</div>
					<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
					<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
					<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
				</div>

				<a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

			</div>
		</section>


		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap nopadding">
				
			

				<div class="section nomargin nobottompadding">
					<div class="" style="position: absolute; bottom: 0;left: 0; width: 100%;height: 100%;background: transparent url('images/ipad-section.png') bottom right no-repeat;"></div>
					<div class="container clearfix">

						<div class="col_half nobottommargin topmargin-sm">

							<div class="heading-block topmargin-sm bottommargin-sm">
								<h3><span>Ocean Vantage </span>Holdings Berhad</h3>
							</div>

							<p>Ocean Vantage Holdings Berhad (“OVH” or “Company”) was incorporated in Malaysia on 10 October 2018 under Companies Act 2016 as a private limited company and subsequently converted to a public limited company on 10 April 2019 and assumed its present name. OVH was incorporated to facilitate the listing of Ocean Vantage Engineering Sdn Bhd (“OVE”) and Ocean Vantage Inspection Testing Sdn Bhd (“OVIT”) on the ACE Market of Bursa Malaysia Securities Berhad (“Bursa Securities”) (“Listing”).</p>
							
 
							<a href="{{route('overview')}}" class="button button-border button-rounded button-large button-ovh noleftmargin">WHO ARE WE</a>

						</div>
						<div class="col_half bottommargin-lg topmargin-sm col_last center">
							
							<img src="images/ovh-title.png"  class="center-block">
							<img src="images/video-btn.png"  class="center-block topmargin-sm">
                                <div class="row center" style="display: flex; justify-content: center">                                   
                                <a href="#myModal1" data-lightbox="inline" class="button button button-rounded button-fill button-ovh button-small"><span>Video 1</span></a>
                                <a href="#myModal2" data-lightbox="inline" class="button button button-rounded button-fill button-ovh button-small"><span>Video 2</span></a>
                                <a href="#myModal3" data-lightbox="inline" class="button button button-rounded button-fill button-ovh button-small"><span>Video 3</span></a>
                                <a href="#myModal4" data-lightbox="inline" class="button button button-rounded button-fill button-ovh button-small"><span>Video 4</span></a>
                                </div> 
                    
                            
  
                            <!-- Modal -->
					<!-- Modal -->
					<div class="modal1 mfp-hide" id="myModal1">
						<div class="block divcenter" style="max-width: 1000px;">
							<div class="center clearfix" style="padding: 0px;">
								
                                <iframe src="https://player.vimeo.com/video/380667563?badge=0" width="600" height="360" frameborder="0" allow="autoplay; fullscreen"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</div>
						</div>
					</div>
                   
                    <div class="modal1 mfp-hide" id="myModal2">
						<div class="block divcenter" style=" max-width: 1000px;">
							<div class="center clearfix" style="padding: 0px;">
								<iframe src="https://player.vimeo.com/video/380667586?badge=0" frameborder="0" width="600" height="360"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div> 
						</div>
					</div>
                            <div class="modal1 mfp-hide" id="myModal3">
						<div class="block divcenter" style="max-width: 1000px;">
							<div class="center clearfix" style="padding: 0px;">
								<iframe src="https://player.vimeo.com/video/380667615?badge=0" frameborder="0" width="600" height="360"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                
							</div>
						</div>
					</div>
                            <div class="modal1 mfp-hide" id="myModal4">
						<div class="block divcenter" style="max-width: 1000px;">
							<div class="center clearfix" style="padding: 0px;">
								<iframe src="https://player.vimeo.com/video/380667640?badge=0" frameborder="0" width="600" height="360"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                
                                
							</div>
						</div>
					</div>

                            <!-- End Modal -->


						</div>
					</div>
				</div>
				
				<div class="section nomargin noborder" style="padding: 100px 0;  background-image: url('images/bg-wwd.jpg'); background-position: center center;">
					<div class="container center clearfix">

						<div class="heading-block nobottommargin">
							<h2><span>WHAT WE DO</span></h2>
						</div>
						    <p class="topmargin-sm center">We support upstream and downstream activities in the O&G industry. <br>
								Presently, we primarily operate in the upstream O&G segment.  Our customers comprise international drilling contractors,<br>
								oilfield service companies, international and national oil companies. 
							</p>
                                        
						<a href="{{route('upstream')}}" class="button button-border button-rounded button-fill button-ovh button-large"><span>Upstream</span></a>
						<a href="{{route('downstream')}}" class="button button-border button-rounded button-fill button-ovh button-large"><span>Downstream</span></a>

					</div>
				</div>

				
				
			 

			</div>		 
		</section><!-- #content end -->
				  <!-- Bursa
		============================================= -->	    
         <div id="page-menu" class="no-sticky">
			<div id="page-menu-wrap">
				<div class="container clearfix">
					<div class="menu-title"><span>Stock</span> Quote : -</div>
				</div>
			</div>
		</div>
@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop