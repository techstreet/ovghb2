@extends('layouts._partials.app')

@section('body')
		<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header-location.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
            
		</section>
        
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Location</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->

		<section id="slider" class="slider-element full-screen with-header swiper_wrapper clearfix">

			<div class="slider-parallax-inner">

				<div class="swiper-container swiper-parent">

					<div class="swiper-wrapper">
						<div class="swiper-slide dark" style="background-image: url('images/sarawak.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map">
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Bintulu, Sarawak <span>Headquarter</span></h4>
                                             <address>
                                                Lot 2587, 2nd Floor,<br>
                                                No.13, Kidurong Gateway,<br>
                                                97000, Bintulu, Sarawak,<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +6086-252 228<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +6086-252 227<br>
	                                        </div>
								</div>
							</div>
						</div>
						<div class="swiper-slide dark" style="background-image: url('images/kl.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map">
									
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Puchong, Selangor <span>Corporate Office</span></h4>
                                             
                                           <address>
                                                F-01-03, Level 3, Black F,<br>
                                                Setia Walk, Persiaran Wawasan,<br>
                                                Pusat Bandar Puchong,<br>
                                                47160 Puchong, Selangor,<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +603-5886 2555<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +603-5886 5022<br>
	                                        </div>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('images/jb.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map"> 
									
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Johor Bharu <span>Branch</span></h4>
                                             <address>
                                                No. 657, Jalan Idaman 3/4,<br>
                                                Senai Industrial Park,<br>
                                                Taman Desa Idaman,<br>
                                                81400, Kulai, Johor,<br>
                                                Malaysia.<br>

                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +607-5951 896<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +607-5951 896<br>
	                                        </div>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('images/labuan.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map">
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Labuan, Sabah <span>Branch</span></h4>
                                             <address>
                                                Lot 6, 2nd Floor, Wisma Wong Wo Lo,<br>
                                                 Jalan Tun Mustapha, <br>
                                                 87000 Labuan F.T<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +6087-408 377<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +6087-408 377<br>
	                                        </div>
                                </div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('images/miri.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map">
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Miri, Sarawak <span>Branch</span></h4>
                                             <address>
                                              
                                                Lot 5570, 1st Floor, Jalan Desa Pujut,<br>
                                                Desa Pujut Shophouse,<br>
                                                Pusat Bandar Baru Permyjaya,<br>
                                                98100, Miri, Sarawak,<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +6085-491 780<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +6085-491 785<br>
	                                        </div>
								</div>
							</div>
						</div>

					</div>

					<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
					<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
					<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
				</div>

			</div>

		</section>

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">

				<div class="row">
						<div class="col_half text-exlarge">
						  				 <!--img src="images/logo.png"-->
								 		
										 <address>
										 <h4 class="title-block bottommargin-sm">Miri, Sarawak <span>Headquarter</span></h4>
										   Lot 5570, 1st Floor, Jalan Desa Pujut,
										   Desa Pujut Shophouse,<br>
										   Pusat Bandar Baru Permyjaya, 98100, Miri, Sarawak, Malaysia.
									   </address>
									   <abbr title="Phone Number"><strong>Phone:</strong></abbr>  +6085-491 780<br>
									   <abbr title="Fax"><strong>Fax:</strong></abbr> +6085-491 785<br>
									   <abbr title="Email Address"><strong>Email:</strong></abbr> info@ovbhd.com
						
						</div>
					   
	

							<div class="col_half col_last text-exlarge">
												<address>
												<h4 class="title-block bottommargin-sm">Puchong, Selangor <span>Corporate Office</span></h4>
													F-01-03, Level 3, Black F,
													Setia Walk, Persiaran Wawasan,<br>
													Pusat Bandar Puchong,
													47160 Puchong, Selangor, Malaysia.
												</address>
												<abbr title="Phone Number"><strong>Phone:</strong></abbr> +603-5886 2555<br>
												<abbr title="Fax"><strong>Fax:</strong></abbr> +603-5886 5022<br>
							</div>
						</div>

						<div class="row">
							<div class="col_one_third">
													<h4 class="title-block bottommargin-sm">Johor Bharu <span>Branch</span></h4>
													<address>
														No. 657, Jalan Idaman 3/4,
														Senai Industrial Park,<br>
														Taman Desa Idaman,
														81400, Kulai, Johor,<br>
														Malaysia.<br>

													</address>
													<abbr title="Phone Number"><strong>Phone:</strong></abbr> +607-5951 896<br>
													<abbr title="Fax"><strong>Fax:</strong></abbr> +607-5951 896<br>
													
													
								</div>
							<div class="col_one_third">
								<h4 class="title-block bottommargin-sm">Labuan, Sabah <span>Branch</span></h4>
													<address>
														Lot 6, 2nd Floor,
														Wisma Wong Wo Lo,<br>
														Jalan Tun Mustapha, 
														87000 Labuan F.T<br>
														Malaysia.<br>
													</address>
													<abbr title="Phone Number"><strong>Phone:</strong></abbr> +6087-408 377<br>
													<abbr title="Fax"><strong>Fax:</strong></abbr> +6087-408 377<br>
								</div>
							
								<div class="col_one_third col_last">
								<h4 class="title-block bottommargin-sm">Bintulu, Sarawak <span>Branch</span></h4>
												<address>
														Lot 2587, 2nd Floor,
														No.13, Kidurong Gateway,<br>
														97000, Bintulu, Sarawak,
														Malaysia.<br>
													</address>
													<abbr title="Phone Number"><strong>Phone:</strong></abbr> +6086-252 228<br>
													<abbr title="Fax"><strong>Fax:</strong></abbr> +6086-252 227<br>
							</div>
						</div>
					
					<!-- .postcontent end -->
                    
				</div>
			</div>
                   
            <div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
            </div>

		</section><!-- #content end -->
        
     

@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
