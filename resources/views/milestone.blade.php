@extends('layouts._partials.app')

@section('body')
		<!-- Page Title ============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class=" topmargin-lg">
			</div>
		</section>

		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Who Are We / Corporate Milestone</h1> 
				
			</div>

		</section><!-- #page-title end -->

		<!-- Content============================================= -->				
		<section id="content" class="parallax" style="background-image: url(images/milestone-bg.jpg);">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
				<div class="col_full">
						<div class="heading-block center">
							<h4>Coprorate <span>Milestone</span></h4>
						</div>	
				</div>
				<div class="col_full">
						<!-- Posts
					============================================= -->
					<div id="posts" class="post-grid grid-container post-masonry post-timeline grid-2 clearfix">

						<div class="timeline-border"></div>

						<div class="entry entry-date-section notopmargin"><span>Year 2011</span></div>

						<div class="entry clearfix">
							<div class="entry-timeline">
								<div class="timeline-divider"></div>
							</div>
							<div class="entry-image">
								<a href="images/timeline1.jpg" data-lightbox="image"><img class="image_fade" src="images/timeline1.jpg"></a>
							</div>
							<div class="entry-title">
								<h2><span>Incorporation of OVE</span></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li class="year"><i class="icon-calendar3"></i> 2011</li>
							</ul>
							<div class="entry-content timeline-paragraph p-b-100 milestone-left">
								<p>-OVE set up office in Miri, Sarawak. </p>
								<p>-OVE secured first contract for the provision of manpower and equipment to carry out engineering and fabrication works in Labuan.</p>
							</div>
						</div>

						<div class="entry entry-date-section"><span>Year 2012</span></div>

						<div class="entry clearfix">
						</div>


						<div class="entry clearfix">
							<div class="entry-timeline">
								<div class="timeline-divider"></div>
							</div>
							<div class="entry-image">
								<a href="images/milestone-pic-abs.png" data-lightbox="image"><img class="image_fade" src="images/milestone-pic-abs.png"></a>
							</div>
							<div class="entry-title">
								<h2><span>Incorporation of OVIT</span></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li class="year"><i class="icon-calendar3"></i> 2012</li>
							</ul>
							<div class="entry-content timeline-paragraph p-b-100 milestone-right">
								<p>-OVIT was recognised as an approved external specialist for hull gauging services by the American Bureau of Shipping (ABS)</p>
								<p>-OVE was registered with the CIDB as a Grade 3 contractor</p>
							</div>
						</div>


						<div class="entry entry-date-section"><span>Year 2013</span></div>


						<div class="entry clearfix">
							<div class="entry-timeline">
								<div class="timeline-divider"></div>
							</div>
							<div class="entry-image">
								<a href="images/cidb.png" data-lightbox="image"><img class="image_fade" src="images/cidb.png"></a>
							</div>
							<div class="entry-title">
								<h2><span>-OVE and OVIT set up office in Puchong</span></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li class="year"><i class="icon-calendar3"></i> 2013</li>
							</ul>
							<div class="entry-content timeline-paragraph p-b-100 milestone-left">
								<p>-OVE’s CIDB license was upgraded to Grade 5</p>
							</div>
						</div>

						<div class="entry entry-date-section"><span>Year 2014</span></div>

						<div class="entry clearfix">
						</div>


						<div class="entry clearfix">
							<div class="entry-timeline">
								<div class="timeline-divider"></div>
							</div>
							<div class="entry-image">
								<a href="images/timeline4.jpg" data-lightbox="image"><img class="image_fade" src="images/timeline4.jpg"></a>
							</div>
							<div class="entry-title">
								<h2><span>OVE’s CIDB license was upgraded to Grade 7</span></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li class="year"><i class="icon-calendar3"></i> 2014</li>
							</ul>
							<div class="entry-content timeline-paragraph p-b-100 milestone-right">
								<p>-OVE obtained PETRONAS licence</p>
								<p>-OVE set up fabrication yard in Senai, Johor</p>
								<p>-OVE set up office in Bintulu, Sarawak</p>
								<p>-OVE was appointed as the exclusive agent and partner for Noble for the provision of drilling rigs</p>
							</div>
						</div>

						<div class="entry entry-date-section"><span>Year 2015</span></div>

						

						<div class="entry clearfix">
							<div class="entry-timeline">
								<div class="timeline-divider"></div>
							</div>
							<div class="entry-image">
								<a href="images/timeline4.jpg" data-lightbox="image"><img class="image_fade" src="images/timeline4.jpg"></a>
							</div>
							<div class="entry-title">
								<h2><span>OVE was approved by Det Norske Veritas</span></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li class="year"><i class="icon-calendar3"></i> 2015</li>
							</ul>
							<div class="entry-content timeline-paragraph p-b-100 milestone-left">
								<p>OVE was approved by Det Norske Veritas for meeting its standards for the manufacture of offshore containers in relation to its fabrication works</p>
							</div>
						</div>

						<div class="entry entry-date-section"><span>Year 2016</span></div>

						<div class="entry clearfix">
						</div>



						<div class="entry clearfix">
							<div class="entry-timeline">
								<div class="timeline-divider"></div>
							</div>
							<div class="entry-image">
								<a href="images/milestone-pic-veritas.png" data-lightbox="image"><img class="image_fade" src="images/milestone-pic-veritas.png"></a>
							</div>
							<div class="entry-title">
								<h2><span>OVE was certified ISO 9001:2015 compliant by Bureau Veritas Certification (Malaysia) Sdn Bhd </span></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li class="year"><i class="icon-calendar3"></i> 2016</li>
							</ul>
							<div class="entry-content timeline-paragraph p-b-100 milestone-left">
								<p>OVIT was certified to conduct thickness measurement of hull structures by Bureau Veritas Certification (Malaysia) Sdn Bhd</p>
							</div>
						</div>

						<div class="entry entry-date-section"><span>Year 2018</span></div>



						<div class="entry clearfix">
							<div class="entry-timeline">
								<div class="timeline-divider"></div>
							</div>
							<div class="entry-image">
								<a href="images/milestone-pic-noble.png" data-lightbox="image"><img class="image_fade" src="images/milestone-pic-noble.png"></a>
							</div>
							<div class="entry-title">
								<h2><span>Further extended for 5 years</span></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li class="year"><i class="icon-calendar3"></i> 2018</li>
							</ul>
							<div class="entry-content timeline-paragraph p-b-100 milestone-right">
								<p>OVE’s appointment as the exclusive agent and partner for Noble was further extended for 5 years</p>
							</div>
						</div>

						<div class="entry entry-date-section"><span>Year 2019</span></div>


						<div class="entry clearfix">
						</div>

						<div class="entry clearfix">
							<div class="entry-timeline">
								<div class="timeline-divider"></div>
							</div>
                            
							<div class="entry-image">
								<a href="images/milestone-pic-classnk.png" data-lightbox="image"><img class="image_fade" src="images/milestone-pic-classnk.png"></a>
							</div>
							<div class="entry-title">
								<h2><span>OVIT was approved to engage with Nippon Kaiji Kyokai</span></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li class="year"><i class="icon-calendar3"></i> 2019</li>
							</ul>
							<div class="entry-content timeline-paragraph p-b-100 milestone-left">
								<p>OVIT was approved to engage in the provision of thickness measurements under the rules for approval of manufacturers and service suppliers of the society on ships by Nippon Kaiji Kyokai</p>
							</div>
						</div>

					</div><!-- #posts end -->
				</div>
				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->
@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')

<script>

jQuery(window).on( 'load', function(){

	var $container = $('#posts');

	$container.isotope({
		itemSelector: '.entry',
		masonry: {
			columnWidth: '.entry:not(.entry-date-section)'
		}
	});

	$container.infiniteScroll({
		path: '.load-next-posts',
		button: '.load-next-posts',
		scrollThreshold: false,
		history: false,
		status: '.page-load-status'
	});

	$container.on( 'load.infiniteScroll', function( event, response, path ) {
		var $items = $( response ).find('.entry');
		// append items after images loaded
		$items.imagesLoaded( function() {
			$container.append( $items );
			$container.isotope( 'insert', $items );
			setTimeout( function(){
				$container.isotope('layout');
				SEMICOLON.initialize.resizeVideos();
				SEMICOLON.widget.loadFlexSlider();
				SEMICOLON.widget.masonryThumbs();
			}, 1000 );
			setTimeout( function(){
				SEMICOLON.initialize.blogTimelineEntries();
			}, 1500 );
		});
	});

	setTimeout( function(){
		SEMICOLON.initialize.blogTimelineEntries();
	}, 2500 );

	$(window).resize(function() {
		$container.isotope('layout');
		setTimeout( function(){
			SEMICOLON.initialize.blogTimelineEntries();
		}, 2500 );
	});

});

</script>
@stop