@extends('layouts._partials.app')

@section('body')
	<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Newsroom</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
							
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
				<div class="col_full">
						<div class="heading-block nobottomborder">
							<h4>NEWS<span>ROOM</span></h4>
						</div>	
				</div>
					<div class="col_full">
						<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
					 </div>
				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->
        
        
        

@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
