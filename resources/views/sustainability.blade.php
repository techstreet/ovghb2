@extends('layouts._partials.app')

@section('body')
		<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Sustainability</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		
        <section id="section-testimonials" class="page-section nomargin section parallax" style="background-image: url('images/hse.jpg'); padding: 150px 0; baackground-size: cover;" >

					<div class="container clearfix">

						<div class="col_half nobottommargin">
                        
                         <h1 class="text-exlarge3"><span>SAFETY</span></h1>
                          <h1 class="text-exlarge3">EFFICIENCY</h1> 
                          <h1 class="text-exlarge3">COMPETENCY</h1>  
                        <h2><span>SAFETY IS ALWAYS FIRST WITH US</span></h2>
                            
                        </div>

						<div class="col_half nobottommargin col_last">

							<div class="heading-block center">
								<h1>UNCOMPROMISING <span>on</span> </h1>
								<span>HEALTH, SAFETY &amp; QUALITY</span>
							</div>

							<div class="fslider testimonial testimonial-full nobgcolor noborder noshadow nopadding" data-arrows="false">
								<div class="flexslider">
									<div class="slider-wrap">
										<div class="slide">
											<div class="testi-content">
												<p>Delivering top quality service means a lot to us, but safety of our people are our highest priority</p>
												
											</div>
										</div>
										<div class="slide">
											<div class="testi-content">
												<p>Our people are our most valuable assets in growing our company and delivering top notch service to our clients. </p>
											</div>
										</div>
										
									</div>
								</div>
							</div>

						</div>

					</div>
        
        </section>
        <section id="content">
            <div class="content-wrap nobottompadding">
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>
            </div>
        </section>
        <!-- #content end -->

        

@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
