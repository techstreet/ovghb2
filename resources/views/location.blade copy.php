@extends('layouts._partials.app')

@section('body')
		<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header-location.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
            
		</section>
        
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Location</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->

		<section id="slider" class="slider-element full-screen with-header swiper_wrapper clearfix">

			<div class="slider-parallax-inner">

				<div class="swiper-container swiper-parent">

					<div class="swiper-wrapper">
						<div class="swiper-slide dark" style="background-image: url('images/kl.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map"><!-- 
									<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">Create just what you need for your Perfect Website. Choose from a wide range of Elements &amp; simply put them on our Canvas.</p> -->
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Puchong, Selangor</h4>
                                             
                                           <address>
                                                F-01-03, Level 3, Black F,<br>
                                                Setia Walk, Persiaran Wawasan,<br>
                                                Pusat Bandar Puchong,<br>
                                                47160 Puchong, Selangor,<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +603-5886 2555<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +603-5886 5022<br>
	                                        </div>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('images/jb.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map"><!-- 
									<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">Create just what you need for your Perfect Website. Choose from a wide range of Elements &amp; simply put them on our Canvas.</p> -->
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Johor Bharu</h4>
                                             <address>
                                                No. 657 Jalan Idaman 3/4,<br>
                                                Senai Industrial Park,<br>
                                                Taman Desa Idaman,<br>
                                                81400, Kulai, Johor,<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +607-5951 896<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +607-5951 896<br>
	                                        </div>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('images/labuan.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map"><!-- 
									<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">Create just what you need for your Perfect Website. Choose from a wide range of Elements &amp; simply put them on our Canvas.</p> -->
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Labuan, Sabah</h4>
                                             <address>
                                                Lot 6, 2nd Floor, Wisma Wong Wo Lo,<br>
                                                 Jalan Tun Mustapha, <br>
                                                 87000 Labuan F.T<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +608-7408 377<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +608-7408 377<br>
	                                        </div>
                                </div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('images/miri.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map"><!-- 
									<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">Create just what you need for your Perfect Website. Choose from a wide range of Elements &amp; simply put them on our Canvas.</p> -->
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Miri, Sarawak</h4>
                                             <address>
                                              
                                                Lot 5570, 1st Floor, Jalan Desa Pujut,<br>
                                                Desa Pujut Shophouse,<br>
                                                Pusat Bandar Baru Permyjaya,<br>
                                                98100, Miri, Sarawak,<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +608-5491 780<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +608-5491 785<br>
	                                        </div>
								</div>
							</div>
						</div>

						<div class="swiper-slide dark" style="background-image: url('images/sarawak.jpg');">
							<div class="container clearfix">
								<div class="slider-caption slider-caption-left map-pin">
									<img src="images/pin.png" data-animate="fadeInDown" class="slider-pin">
									<img src="images/map.png" data-animate="fadeInUp" class="topmargin slider-map"><!-- 
									<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">Create just what you need for your Perfect Website. Choose from a wide range of Elements &amp; simply put them on our Canvas.</p> -->
								</div>
								<div class="slider-caption address-box" data-animate="fadeIn" data-delay="300">
									<h4 class="title-block bottommargin-sm">Bintulu, Sarawak</h4>
                                             <address>
                                                Lot 2587, 2nd Floor,<br>
                                                No.13, Kidurong Gateway,<br>
                                                97000, Bintulu, Sarawak,<br>
                                                Malaysia.<br>
                                            </address>
                                            <div class="xs-none xss-phone">
	                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +608-6252 228<br>
	                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +608-6252 227<br>
	                                        </div>
								</div>
							</div>
						</div>
					</div>

					<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
					<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
					<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
				</div>

			</div>

		</section>

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">

					<!-- Postcontent
					============================================= -->
					<div class="postcontent nobottommargin">
						<div class="form-widget">

							<div class="form-result"></div>

							<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/form.php" method="post">

								<div class="form-process"></div>

								<div class="col_one_third">
									<label for="template-contactform-name">Name <small>*</small></label>
									<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
								</div>

								<div class="col_one_third">
									<label for="template-contactform-email">Email <small>*</small></label>
									<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
								</div>

								<div class="col_one_third col_last">
									<label for="template-contactform-phone">Phone</label>
									<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
								</div>

								<div class="clear"></div>

								<div class="col_two_third">
									<label for="template-contactform-subject">Subject <small>*</small></label>
									<input type="text" id="template-contactform-subject" name="subject" value="" class="required sm-form-control" />
								</div>

								<div class="col_one_third col_last">
									<label for="template-contactform-service">Services</label>
									<select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">
										<option value="">-- Select One --</option>
                                        <option value="enquiry">Enquiry</option>
										<option value="upstream/downstream">Upstream/Downstream</option>
										
									</select>
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-contactform-message">Message <small>*</small></label>
									<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
								</div>

								<div class="col_full hidden">
									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
								</div>

								<div class="col_full">
									<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
								</div>

								<input type="hidden" name="prefix" value="template-contactform-">

							</form>
						</div>
					</div><!-- .postcontent end -->

					<!-- Sidebar
					============================================= -->
					<div class="sidebar col_last nobottommargin">
                        
                                  <img src="images/logo.png" >
                                           <address>
                                                <strong>Headquarters:</strong><br>
                                                F-01-03, Level 3, Black F,<br>
                                                Setia Walk, Persiaran Wawasan,<br>
                                                Pusat Bandar Puchong,<br>
                                                47160 Puchong, Selangor,<br>
                                                Malaysia.<br>
                                            </address>
                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +603-5886 2555<br>
                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +603-5886 5022<br>
                                            <abbr title="Email Address"><strong>Email:</strong></abbr> info@ovbhd.com
                                            
                            
                             <div class="widget noborder notoppadding">
                                    <a href="#" class="social-icon si-small si-dark si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-small si-dark si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                </div>  
                            
                                     <!-- end slide address --->
							
                               
                        
                        

					</div><!-- .sidebar end -->
                    <div class="content nobottommargin">
                    <div class="line"></div>
                       <div class="col_one_fourth">
                                            <h4 class="title-block bottommargin-sm">Johor Bharu</h4>
                                             <address>
                                                No. 657 Jalan Idaman 3/4,<br>
                                                Senai Industrial Park,<br>
                                                Taman Desa Idaman,<br>
                                                81400, Kulai, Johor,<br>
                                                Malaysia.<br>
                                            </address>
                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +607-5951 896<br>
                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +607-5951 896<br>
                                            
                                            
                        </div>
                       <div class="col_one_fourth">
                             
                           <h4 class="title-block bottommargin-sm">Labuan, Sabah</h4>
                                             <address>
                                                 Lot 6, 2nd Floor,<br>
                                                 Wisma Wong Wo Lo,<br>
                                                 Jalan Tun Mustapha, <br>
                                                 87000 Labuan F.T<br>
                                                Malaysia.<br>
                                            </address>
                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +608-7408 377<br>
                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +608-7408 377<br>
                                            
                        
                         
                        </div>
                       <div class="col_one_fourth">
                           <h4 class="title-block bottommargin-sm">Miri, Sarawak</h4>
                               <address>
                                              
                                                Lot 5570, 1st Floor, Jalan Desa Pujut,<br>
                                                Desa Pujut Shophouse,<br>
                                                Pusat Bandar Baru Permyjaya,<br>
                                                98100, Miri, Sarawak,<br>
                                                Malaysia.<br>
                                            </address>
                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +608-5491 780<br>
                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +608-5491 785<br>
                                            
                        
                       
                        </div>
                       <div class="col_one_fourth col_last">
                           <h4 class="title-block bottommargin-sm">Bintulu, Sarawak</h4>
                                           <address>
                                                Lot 2587, 2nd Floor,<br>
                                                No.13, Kidurong Gateway,<br>
                                                97000, Bintulu, Sarawak,<br>
                                                Malaysia.<br>
                                            </address>
                                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +608-6252 228<br>
                                            <abbr title="Fax"><strong>Fax:</strong></abbr> +608-6252 227<br>
                                            
                        
                        
                        </div>
                    
                    </div>
                    
				</div>
			</div>
                   
            <div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
            </div>

		</section><!-- #content end -->
        
     

@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
