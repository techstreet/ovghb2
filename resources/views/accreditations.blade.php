@extends('layouts._partials.app')

@section('body')

<!-- Page Title
		============================================= -->
		<section id="page-title-mini" class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Who Are We / Accerditations</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
							
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
                    <div class="col_full">
						<div class="heading-block center">
							<h4>ACCREDITATIONS</h4>
						</div>	
				    </div>
                    
                    <div class="section">
                       <div class="row grid-container" data-layout="masonry" style="overflow: visible">
                        
						<div class="col-lg-4 mb-4">
							<div class="flip-card text-center">
								<div class="flip-card-front dark" data-height-xl="250" style="background-image: url('images/thumb-cert.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
												<h3 class="card-title">Ocean Vantage Engineering Sdn bhd</h3>
												<p class="card-text t400 text-center">ISO9001 2019 - STANDARDS</p>
                                                <a href="#" class="btn btn-outline-light mt-2">View</a>
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-info no-after"  data-height-xl="250">
									<div class="flip-card-inner"><!-- 
										<p class="mb-2 text-white text-center"><img src="images/icon-pdf.png"><br>378KB</p> -->
										<a href="download/ove-iso-standards.jpg" data-lightbox="image">
											<p class="btn btn-outline-light mt-2">View</p>
										</a>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-4 mb-4">
							<div class="flip-card text-center top-to-bottom">
								<div class="flip-card-front dark" data-height-xl="250" style="background-image: url('images/thumb-cert.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
												<h3 class="card-title">Ocean Vantage Engineering Sdn bhd</h3>
												<p class="card-text t400 text-center">CIDB CERT 2018</p>
                                                <a href="#" class="btn btn-outline-light mt-2">View</a>
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-info no-after"  data-height-xl="250">
									<div class="flip-card-inner">
										<div class="clearfix" data-big="3" data-lightbox="gallery">
											<a href="download/ove-cidb-1.jpg" data-lightbox="gallery-item">
												<p class="btn btn-outline-light mt-2">View</p>
											</a>
											<a href="download/ove-cidb-2.jpg" data-lightbox="gallery-item">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>

                      <!--large-->
						<div class="col-lg-4 mb-4">
							<div class="flip-card text-center">
								<div class="flip-card-front dark" data-height-xl="520" style="background-image: url('images/thumb-cert.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
												<img src="images/icon-avatar.png">
												<h3 class="card-title">Ocean Vantage Inspection Testing  Sdn bhd</h3>
												<p class="card-text t400 text-center">ABS Non-Destructive Examination</p>
                                                <a href="#" class="btn btn-outline-light mt-2">View</a>
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-info no-after" data-height-xl="520">
									<div class="flip-card-inner">
										<div class="clearfix" data-big="3" data-lightbox="gallery">
											<a href="download/ovit-abs-ndt-1.jpg" data-lightbox="gallery-item">
												<p class="btn btn-outline-light mt-2">View</p>
											</a>
											<a href="download/ovit-abs-ndt-2.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ovit-abs-ndt-3.jpg" data-lightbox="gallery-item">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
                     <!--end large -->

						<div class="col-lg-4 mb-4">
							<div class="flip-card text-center top-to-bottom">
								<div class="flip-card-front dark" data-height-xl="250"  style="background-image: url('images/thumb-cert.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
												<h3 class="card-title">Ocean Vantage Engineering Sdn bhd</h3>
												<p class="card-text t400 text-center">ISO9001 2019 - UKAS</p>
                                                <a href="#" class="btn btn-outline-light mt-2">View</a>
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-info no-after" data-height-xl="250">
									<div class="flip-card-inner">
										<a href="download/ove-iso-ukas.jpg" data-lightbox="image">
											<p class="btn btn-outline-light mt-2">View</p>
										</a>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-4 mb-4">
							<div class="flip-card text-center top-to-bottom">
								<div class="flip-card-front dark" data-height-xl="250"  style="background-image: url('images/thumb-cert.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
												<h3 class="card-title">Ocean Vantage Engineering Sdn bhd</h3>
												<p class="card-text t400 text-center">Petronas License</p>
                                                <button type="button" class="btn btn-outline-light mt-2">View</button>
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-info no-after" data-height-xl="250">
									<div class="flip-card-inner">
										<div class="clearfix" data-big="3" data-lightbox="gallery">
											<a href="download/ove-petronas-1.jpg" data-lightbox="gallery-item">
												<p class="btn btn-outline-light mt-2">View</p>
											</a>
											<a href="download/ove-petronas-2.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-3.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-4.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-5.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-6.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-7.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-8.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-9.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-10.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ove-petronas-11.jpg" data-lightbox="gallery-item">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
                           <!----ovit-->
                           <div class="col-lg-4 mb-4">
							<div class="flip-card text-center top-to-bottom">
								<div class="flip-card-front dark" data-height-xl="250"  style="background-image: url('images/thumb-cert.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
												<h3 class="card-title">Ocean Vantage Inspection Testing Sdn bhd</h3>
												<p class="card-text t400 text-center">ABS Hull Gauging Firm</p>
                                                <button type="button" class="btn btn-outline-light mt-2">View</button>
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-info no-after" data-height-xl="250">
									<div class="flip-card-inner">
										<div class="clearfix" data-big="3" data-lightbox="gallery">
											<a href="download/ovit-hullgauging-1.jpg" data-lightbox="gallery-item">
												<p class="btn btn-outline-light mt-2">View</p>
											</a>
											<a href="download/ovit-hullgauging-2.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ovit-hullgauging-3.jpg" data-lightbox="gallery-item">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
                           <!----ovit-->
                           <div class="col-lg-4 mb-4">
							<div class="flip-card text-center top-to-bottom">
								<div class="flip-card-front dark" data-height-xl="250"  style="background-image: url('images/thumb-cert.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
												<h3 class="card-title">Ocean Vantage Inspection Testing Sdn bhd</h3>
												<p class="card-text t400 text-center">ABS </p>
                                                <button type="button" class="btn btn-outline-light mt-2">View</button>
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-info no-after" data-height-xl="250">
									<div class="flip-card-inner">
										<div class="clearfix" data-big="3" data-lightbox="gallery">
											<a href="download/ovit-abs-1.jpg" data-lightbox="gallery-item">
												<p class="btn btn-outline-light mt-2">View</p>
											</a>
											<a href="download/ovit-abs-2.jpg" data-lightbox="gallery-item">
											</a>
											<a href="download/ovit-abs-3.jpg" data-lightbox="gallery-item">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
                           <!----ovit-->
                           <div class="col-lg-4 mb-4">
							<div class="flip-card text-center top-to-bottom">
								<div class="flip-card-front dark" data-height-xl="250"  style="background-image: url('images/thumb-cert.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
												<h3 class="card-title">Ocean Vantage Inspection Testing Sdn bhd</h3>
												<p class="card-text t400 text-center">Nippon Kaiji Kyokai</p>
                                                <button type="button" class="btn btn-outline-light mt-2">View</button>
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-info no-after" data-height-xl="250">
									<div class="flip-card-inner">
										<div class="clearfix" data-big="3" data-lightbox="gallery">
											<a href="download/ovit-nky-1.jpg" data-lightbox="gallery-item">
												<p class="btn btn-outline-light mt-2">View</p>
											</a>
											<a href="download/ovit-nky-2.jpg" data-lightbox="gallery-item">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
                           
                           
                       
                        
					</div>
                     
                    </div>
				</div> <!---end container -->
                
                
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->

@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
