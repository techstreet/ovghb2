@extends('layouts._partials.app')

@section('body')
		<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Sitemap</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
							
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container bottommargin-lg">
                    
                    <div class="section notoppadding notopmargin">
                        <div class="col_one_fourth nobottommargin">

                            <h4 class="nomargin">Home</h4>

                            <ul class="iconlist nobottommargin">
                                <li><i class="icon-file-alt"></i><a href="{{route('index')}}">Main Page</a></li>
                            </ul>
                             <div class="line bottommargin-sm topmargin-sm"></div>
                             <h4 class="nomargin">Sustainability</h4>

                            <ul class="iconlist nobottommargin">
                                <li><i class="icon-file-alt"></i><a href="{{route('sustainability')}}">Sustainability</a></li>
                            </ul>
                               <div class="line bottommargin-sm topmargin-sm"></div>

                            <h4 class="nomargin">Contact Us</h4>

                            <ul class="iconlist nobottommargin">
                                <li><i class="icon-file-alt"></i><a href="{{route('location')}}">Location</a></li>
                            </ul>
                        </div>

                        <div class="col_one_fourth nobottommargin">

                            <h4 class="nomargin">Who Are We</h4>

                            <ul class="iconlist nobottommargin">
                                <li><i class="icon-file-alt"></i><a href="{{route('overview')}}">Overview</a></li>
                                <li><i class="icon-file-alt"></i><a href="{{route('milestone')}}">Corporate Milestone</a></li>
                                <li><i class="icon-file-alt"></i><a href="{{route('boardofdirector')}}">Board of Director</a></li>
                                <li><i class="icon-file-alt"></i><a href="{{route('managementteam')}}">Management Team</a></li>
                                <li><i class="icon-file-alt"></i><a href="{{route('location')}}">Corporate Governance</a></li>
                                <li><i class="icon-file-alt"></i><a href="{{route('accreditations')}}">Accreditations</a></li>
                                <li><i class="icon-file-alt"></i><a href="{{route('groupstructure')}}">Group Structure</a></li>
                            </ul>

                        </div>

                        <div class="col_one_fourth nobottommargin">

                            <h4 class="nomargin">What We Do</h4>

                            <ul class="iconlist nobottommargin">
                                <li><i class="icon-file-alt"></i><a href="{{route('upstream')}}">Upstream</a></li>
                                <li><i class="icon-file-alt"></i><a href="{{route('downstream')}}">Downstream</a></li>
                            </ul>


                        </div>

                        <div class="col_one_fourth nobottommargin col_last">

                            <h4 class="nomargin">Investors Relations</h4>

                            <ul class="iconlist nobottommargin">
                                <li><i class="icon-file-alt"></i><a href="#">Financial Calendar</a></li>
                                <li><i class="icon-file-alt"></i><a href="#">Shares</a></li>
                                <li><i class="icon-file-alt"></i><a href="#">Reports &amp; Announcements</a></li>
                                <li><i class="icon-file-alt"></i><a href=#">Enews Subscription</a></li>
                            </ul>

                            <div class="line bottommargin-sm topmargin-sm"></div>

                            <h4 class="nomargin">Newsroom</h4>

                            <ul class="iconlist nobottommargin">
                                <li><i class="icon-file-alt"></i><a href="#">Bursa Announcement</a></li>
                            </ul>

                        </div>
                        
                        <div class="col_full">&nbsp;</div>
                        
                    </div>
                    
				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->

        
@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
