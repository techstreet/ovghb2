@extends('layouts._partials.app')

@section('body')
	<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class=" topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Who Are We / Management Team</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
							
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
					<div class="col_full">
							<div class="heading-block center">
								<h4>MANAGEMENT <span>TEAM</span></h4>
							</div>	
					</div>
					<div class="content-wrap management-team">
						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/director-kenny.png">
							</div>
							<div class="col-md-9">
								<h3>Kenny Ronald Ngalin</h3>
								<h6><span class="position">Managing Director</span></h6>
								<p>Kenny Ronald Ngalin, a Malaysian, aged 43, is our Managing Director. He was appointed to our Board on 1 April 2019. He is our Group’s founder and has spearheaded the business growth of our Group since its inception in 2011. He is principally responsible for overseeing the day-to-day operations and implementation of the overall strategies and corporate direction of our Group.</p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
										<p>Kenny Ronald Ngalin, a Malaysian, aged 43, is our Managing Director. He was appointed to our Board on 1 April 2019. He is our Group’s founder and has spearheaded the business growth of our Group since its inception in 2011. He is principally responsible for overseeing the day-to-day operations and implementation of the overall strategies and corporate direction of our Group.</p>
										<p>He has more than 16 years of working experience in the O&G industry, specialising in health, safety and environment.</p>
										<p>He obtained his Nursing Diploma from Pantai Institute of Health Science & Nursing in 2001 and holds a certificate from The National Examination Board in Occupational Safety & Health (NEBOSH) United Kingdom since 2012. </p>
										<p>He began his oilfield career in 2002 with International SOS (Malaysia) Sdn Bhd, as an offshore medic providing medical care and various health and safety trainings onboard offshore drilling rigs, platform and remote areas. Throughout his tenure, he was involved in various projects and short assignments as Rig Medic to various local and international O&G companies, such as GlobalSantaFe Corporation, Murphy Oil Corp Sarawak Sdn Bhd, Ensco Gerudi (Malaysia) Sdn Bhd, Transocean Inc., Western Geco Inc. and Grant Geophysical Inc. During his assignments and projects, he was assigned to work on various types of drilling rigs, vessels, floating production storage, shipyards and base in Malaysia, Thailand, Vietnam, Brunei, Indonesia and Singapore, Sudan, China and Nigeria.</p>
										<p>In 2005, he left International SOS (Malaysia) Sdn Bhd and joined Tioman Drilling Sdn Bhd -Smedvig S.A as Safety Training Officer. In 2006, he left Tioman Drilling Sdn Bhd -Smedvig S.A and joined Transocean Drilling Sdn Bhd as Rig Safety Training Coordinator. He later joined Seadrill Management (S) Pte Ltd as a Safety Coach in 2007, where he was responsible to ensure that offshore drilling rigs and platform were in compliance with the internal and external governing requirements. </p>
										<p>In 2011, he founded OVE while he was still attached to Seadrill Management (S) Pte Ltd. In 2013, he left Seadrill Management (S) Pte Ltd to focus solely on OVE. </p>
										<p>He presently holds directorship and shareholdings in a number of private limited companies.</p>

									</div>
								</div>
							</div>
						</div>
						<div class="row teambg-right">
							<div class="col-md-9 pull-right">
								<h3>Martin Philip King Ik Piau</h3>
								<h6><span class="position">Executive Director</span></h6>
								<p class="pull-right">Martin Philip King Ik Piau, a Malaysian, aged 47, is our Executive Director. He was appointed to our Board on 1 April 2019. He joined our Group in 2013 and has been instrumental to the business growth and development of our Group since then. He is responsible for overseeing the overall daily operations of our EPC, project management and procurement activities.</p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
									<p>Martin Philip King Ik Piau, a Malaysian, aged 47, is our Executive Director. He was appointed to our Board on 1 April 2019. He joined our Group in 2013 and has been instrumental to the business growth and development of our Group since then. He is responsible for overseeing the overall daily operations of our EPC, project management and procurement activities.</p>
									<p>He graduated with a Bachelor of Chemical Engineering (Gas) from Universiti Teknologi Malaysia in 1997. He then obtained his Masters in Engineering (Gas) from Universiti Teknologi Malaysia in 2002. He has accumulated 18 years of experience in the EPC and project management for the O&G industry. </p>
									<p>He began his career in 2001, as a Project Engineer at Vantage Steel Works Sdn Bhd where he was involved in a piping fabrication, installation and pre-commissioning project for a butanediol plant in Malaysia. In 2002, he left Vantage Steel Works Sdn Bhd and joined Toyo Engineering & Construction Sdn Bhd in 2002 as a Project Engineer and was involved in projects for process engineering design. In 2004, he left Toyo Engineering & Construction Sdn Bhd and joined WHESSOE Sdn Bhd as Project Engineer, where he was involved in various EPCC projects and hydropower plant projects. He was then promoted to Project Manager in 2011. </p>
									<p>In 2013, he left WHESSOE Sdn Bhd and joined OVE in the same year as a General Manager where he was tasked with overseeing operations, engineering and projects. </p>
									<p>He presently holds directorship and shareholdings in several private limited companies.</p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<img src="images/director-martin.png">
							</div>
						</div>
						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/director-yau.png">
							</div>
							<div class="col-md-9">
								<h3>Yau Kah Tak</h3>
								<h6><span class="position">Executive Director</span></h6>
								<p>Yau Kah Tak, a Malaysian, aged 40, is our Executive Director. He was appointed to our Board on 1 April 2019. He joined our Group in 2014 and has been instrumental to the business growth and development of our Group since then. He is responsible for managing and implementing our Group’s business development plans and strategies.</p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
									<p>Yau Kah Tak, a Malaysian, aged 40, is our Executive Director. He was appointed to our Board on 1 April 2019. He joined our Group in 2014 and has been instrumental to the business growth and development of our Group since then. He is responsible for managing and implementing our Group’s business development plans and strategies.    </p>
									<p>He obtained his Bachelor in Mechanical Engineering with honours from Universiti Teknologi Malaysia in 2001. He has 18 years of experience working in the EPC and project management for the O&G industry.</p>
									<p>In 2001, he joined Transocean Inc. under the Rig Engineer Trainee Programme, and was stationed in Brazil. The Rig Engineer Trainee Programme is a 3-year programme that has been designed to accelerate and advance personnel into management roles in the company. Upon completion of the programme in 2004, he assumed the role of Operations Engineer, where he worked closely with rig managers and offshore rig management teams in managing the day to day operations and rig maintenance activities. </p>
									<p>He was subsequently promoted to the position of Regional Operations Engineer – Performance in 2005, where he worked closely with the Director of Performance for Asia Pacific Unit on performance monitoring and improvement activities and operations lateral learning reviews. In 2006, he was promoted to Rig Manager – Asset and in the same year, became Rig Manager – Performance. In 2007, he joined Seadrill Management (S) Pte Ltd as an Operations Engineer where his role involved project management. </p>
									<p>Subsequently, in 2008, he joined Transocean Inc. as Rig Manager – Asset. In 2011, he was promoted to Strategic Sourcing Manager, where he was involved in pricing agreements, contracts management, vendor performance and management. He then joined Shelf Drilling Ventures Malaysia Sdn Bhd as a Rig Manager in 2013, where he was in charge of offshore rig management team and operational performance.</p>
									<p>In 2014, he left Shelf Drilling Ventures Malaysia Sdn Bhd and joined OVE in the same year, as a Business Development Manager. In August 2017, he left our Group to take a 1-year contract as a Rig Manager in charge of offshore rig management team and operational performance with Perisai Drilling Sdn Bhd. He rejoined OVE in September 2018.</p>
									<p>He presently holds directorship and shareholdings in several private limited companies.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row teambg-right">
							<div class="col-md-9 pull-right">
								<h3>Chang Vun Lung</h3>
								<h6><span class="position">Chief Financial Officer</span></h6>
								<p class="pull-right">Chang Vun Lung, a Malaysian, aged 43, is our Chief Financial Officer. He is responsible for overseeing our Group’s overall finance-related functions including monitoring of financial performance and results, financial reporting, treasury management and tax compliance.In 2004, he obtained his professional accounting qualification from the Association of Chartered...</p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
									<p>Chang Vun Lung, a Malaysian, aged 43, is our Chief Financial Officer. He is responsible for overseeing our Group’s overall finance-related functions including monitoring of financial performance and results, financial reporting, treasury management and tax compliance.</p>
									<p>In 2004, he obtained his professional accounting qualification from the Association of Chartered Certified Accountants, United Kingdom. He has been a member of the Malaysian Institute of Accountants since 2005 and a fellow member of the Association of Chartered Certified Accountants, United Kingdom since 2009.</p>
									<p>He began his career as an Audit Associate in BDO Binder (now known as BDO PLT) in 2001, where he led and supervised an audit team, as well as carried out statutory audit assignments and investigation audit. In 2005, he joined Isyoda Corporation Berhad, when the company was still publicly listed on the Main Board of Bursa Securities as an Accountant, where he was responsible for the preparation and analysis of the financial operations of the Group. He was subsequently promoted to Group Accountant in 2007, where he handled the key accounts of the group of companies, including foreign subsidiary companies.</p>
									<p>In 2008, he left Isyoda Corporation Berhad and joined Synergy Business Advisory Sdn Bhd, a business consulting and management company, and its affiliate Synergy Management Solutions Sdn Bhd, a company secretarial services firm, as Director. In 2011, he was appointed as director of Commerce Cube Sdn Bhd, a company involved in company secretarial services. He resigned as a director of Commerce Cube Sdn Bhd in 2012. </p>
									<p>In 2012, he was appointed as director of Infinity Corporate Services Sdn Bhd, a company involved in company secretarial services. In July 2013, Synergy Business Advisory Sdn Bhd and Synergy Management Solutions Sdn Bhd were dissolved and his role as director of these companies ended. He joined OVE as Financial Controller in February 2014. He then resigned as a director of Infinity Corporate Services Sdn Bhd in March 2014. He redesignated to his current position in our Group in January 2019. </p>
									<p>Presently, he is the Independent Non-Executive Director of Focus Dynamic Technologies Berhad, AT Systemization Berhad and Vsolar Group Berhad. He has undertaken to relinquish his directorship in Vsolar Group Berhad and AT Systematization Berhad by 31 December 2019. He is also a director and shareholder in a number of private limited companies. </p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<img src="images/director-chang.png">
							</div>
						</div>
						<div class="row m-b-30 teambg-left">
							<div class="col-md-3">
								<img src="images/director-thomas.png">
							</div>
							<div class="col-md-9">
								<h3>Thomas Jalong</h3>
								<h6><span class="position">General Manager</span></h6>
								<p>Thomas Jalong, a Malaysian, aged 53, is our General Manager. He is responsible for overseeing the overall daily operations of the engineering and design and fabrication activities of our Group. He also oversees the quality control and assurance of the Group. In 1985, he completed his secondary education from Sekolah Menengah Kebangsaan Lutong. </p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
									<p>Thomas Jalong, a Malaysian, aged 53, is our General Manager. He is responsible for overseeing the overall daily operations of the engineering and design and fabrication activities of our Group. He also oversees the quality control and assurance of the Group.</p>
									<p>In 1985, he completed his secondary education from Sekolah Menengah Kebangsaan Lutong. He has 39 years of working experience in the O&G industry. Upon completing his secondary education, he began working as an Assistant Material Coordinator for Bintulu Crude Oil Terminal, Tanjung Kidurong in 1986.</p>
									<p>In 1987, he joined Yong Tze Kiok Sdn Bhd as a Civil Foreman where he was involved in the construction of 2 units of impounding basins and underground insulated drainage piping at the Bintulu Crude Oil Terminal. Subsequently, in 1988, he joined CBI Overseas Inc. as a Non-Destructive Examination Assistant cum Radiography worker, where he carried out radiographic testing, stress-free temperature testing, and visual testing. </p>
									<p>In 1989, he took the role as an Erection Trainee and Blaster Painter at CBI Overseas Inc. and was promoted to Non-Destructive Examination Technician cum Blaster Painter in 1990. Subsequently, in 1992, he was promoted as a Non-Destructive Examination Technician, where he carried out radiographic testing, magnetic testing, penetrant testing, stress-free temperature testing, and visual testing. </p>
									<p>In 1996, he left CBI Overseas Inc. and joined Samsung Heavy Industries (Thailand) Co. Ltd. as a Quality Assurance and Quality Control Supervisor, where he was responsible for controlling and conducting welder’s qualification testing, and inspection test planning. He was then promoted to Site Quality Assurance and Quality Control Manager in 1997, where he oversees the quality assurance department. In 2001, he was promoted to Site Construction Manager. </p>
									<p>In 2003, he left Samsung Heavy Industries (Thailand) Co. Ltd. to join Spheretech Engineering Sdn Bhd as a Site Construction Manager. The following year, he then joined Gobil Corrosion Engineering Sdn Bhd as a Quality Assurance and Quality Control worker cum Senior Blasting and Painting Supervisor, where he was involved in various top side maintenance works for offshore drilling rigs, platforms and floating production storage and offloading vessels, as well as in the management, coordination and training of personnel to ensure that their compliance with the clients’ specifications and requirements.</p>
									<p>In 2012, he joined OVE as Business Development Manager, where he was involved in the bidding of projects as well as marketing. He was then promoted to Technical Manager in 2014, where he was involved in various topside and tanks maintenance works, conducting site surveys, and establishing blasting and painting plans. He was also responsible for crew selection, manpower planning, as well as training and briefing crew members. He assumed his current position in 2017.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->


@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop