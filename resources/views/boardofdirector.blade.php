@extends('layouts._partials.app')

@section('body')

		<!-- Page Title
		============================================= -->
		<section id="page-title-mini" class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
		</section>
        
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Who Are We / Board of Director</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->					
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
				<div class="col_full">
						<div class="heading-block center">
							<h4>BOARD OF <span>DIRECTOR</span></h4>
						</div>	
				</div>

					<div class="content-wrap director">
						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/nor-azzam.png">
							</div>
							<div class="col-md-9">
								<h3>Nor Azzam Bin Abdul Jalil</h3>
								<h6><span class="position">Independent Non-Executive Chairman</span></h6>
								<p>Nor Azzam bin Abdul Jalil, a Malaysian, aged 54, is our Independent Non-Executive Chairman. He was appointed to our Board on 14 August 2019 and is the Chairman of our Remuneration Committee. He is also a member of our Audit and Risk Management Committee and our Nominating Committee.</p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
										<p>He graduated with a Bachelor of Business Administration (Finance) from George Washington University, USA in 1987. Upon his graduation, he was employed as an Executive Trainee by Bank of Commerce (M) Berhad. In 1991, he was promoted as Assistant Vice President responsible for managing the bank’s Nostro accounts. In 1993, he moved to the corporate banking department as a Credit Officer and was promoted to Head of Japanese Desk in January 1995. </p>
										<p>In 1999, after the merger of Bank of Commerce (M) Berhad and Bank Bumiputra Malaysia Berhad to Bumiputra-Commerce Bank Berhad, he was redesignated as Business Center Manager where he was responsible to set-up and manage a business center in Klang Valley. In 2000, he was seconded to Tokyo, Japan branch of Bumiputra-Commerce Bank Berhad as General Manager to manage the day-to-day operations of the branch. </p>
										<p>In 2005, he returned to Malaysia to become Chief Executive Officer of Commerce Tijari Bank Berhad and was transferred to CIMB Bank Berhad as Regional Director IV (responsible for  the South Selangor and Negeri Sembilan branches) in 2006. He was subsequently promoted to Senior Vice President/Regional Director I (responsible for the Kuala Lumpur branches) in 2010. He remained with the CIMB group and was promoted several times before leaving CIMB Bank Berhad in 2016. His last position with the bank was Acting Head of Consumer Sales and Distribution, responsible for driving retail banking and enterprise banking businesses. He joined Kuwait Finance House (Malaysia) Berhad in the same year as Deputy Chief Executive Officer, where he was responsible for assisting in driving the overall strategic direction of the bank’s business.</p>
										<p>In 2017, he left Kuwait Finance House (Malaysia) Berhad to join his family business, Voxel Imaging Sdn Bhd, a visual effects and production company for film and television as well as end-to-end production for corporate and commercial clients. He currently manages the financial and investment aspects of the company.</p>
										<p>Presently, he sits on the Board of Revenue Group Berhad, a company listed on the ACE Market of Bursa Securities, where he was appointed as an Independent Non-Executive Chairman in December 2017. He also holds directorship in several private limited companies. </p>
									</div>
								</div>
							</div>
						</div>


						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/director-kenny.png" >
							</div>
							<div class="col-md-9">
								<h3>Kenny Ronald Ngalin</h3>
								<h6><span class="position">Managing Director</span></h6>
								<p>Kenny Ronald Ngalin, a Malaysian, aged 43, is our Managing Director. He was appointed to our Board on 1 April 2019. He is our Group’s founder and has spearheaded the business growth of our Group since its inception in 2011. He is principally responsible for overseeing the day-to-day operations and implementation of the overall strategies and corporate direction of our Group.</p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
										<p>He has more than 16 years of working experience in the O&G industry, specialising in health, safety and environment.</p>
										<p>He obtained his Nursing Diploma from Pantai Institute of Health Science & Nursing in 2001 and holds a certificate from The National Examination Board in Occupational Safety & Health (NEBOSH) United Kingdom since 2012.</p>
										<p>He began his oilfield career in 2002 with International SOS (Malaysia) Sdn Bhd, as an offshore medic providing medical care and various health and safety trainings onboard offshore drilling rigs, platform and remote areas. Throughout his tenure, he was involved in various projects and short assignments as Rig Medic to various local and international O&G companies, such as GlobalSantaFe Corporation, Murphy Oil Corp Sarawak Sdn Bhd, Ensco Gerudi (Malaysia) Sdn Bhd, Transocean Inc., Western Geco Inc. and Grant Geophysical Inc. During his assignments and projects, he was assigned to work on various types of drilling rigs, vessels, floating production storage, shipyards and base in Malaysia, Thailand, Vietnam, Brunei, Indonesia and Singapore, Sudan, China and Nigeria.</p>
										<p>In 2005, he left International SOS (Malaysia) Sdn Bhd and joined Tioman Drilling Sdn Bhd -Smedvig S.A as Safety Training Officer. In 2006, he left Tioman Drilling Sdn Bhd -Smedvig S.A and joined Transocean Drilling Sdn Bhd as Rig Safety Training Coordinator. He later joined Seadrill Management (S) Pte Ltd as a Safety Coach in 2007, where he was responsible to ensure that offshore drilling rigs and platform were in compliance with the internal and external governing requirements. </p>
										<p>In 2011, he founded OVE while he was still attached to Seadrill Management (S) Pte Ltd. In 2013, he left Seadrill Management (S) Pte Ltd to focus solely on OVE. </p>
										<p>He presently holds directorship and shareholdings in a number of private limited companies.</p>
									</div>
								</div>
							</div>
						</div>


						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/director-martin.png">
							</div>
							<div class="col-md-9">
								<h3>Martin Philip King Ik Piau</h3>
								<h6><span class="position">Executive Director</span></h6>
								<p>Martin Philip King Ik Piau, a Malaysian, aged 47, is our Executive Director. He was appointed to our Board on 1 April 2019. He joined our Group in 2013 and has been instrumental to the business growth and development of our Group since then. He is responsible for overseeing the overall daily operations of our EPC, project management and procurement activities.</p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
										<p>He graduated with a Bachelor of Chemical Engineering (Gas) from Universiti Teknologi Malaysia in 1997. He then obtained his Masters in Engineering (Gas) from Universiti Teknologi Malaysia in 2002. He has accumulated 18 years of experience in the EPC and project management for the O&G industry. </p>
										<p>He began his career in 2001, as a Project Engineer at Vantage Steel Works Sdn Bhd where he was involved in a piping fabrication, installation and pre-commissioning project for a butanediol plant in Malaysia. In 2002, he left Vantage Steel Works Sdn Bhd and joined Toyo Engineering & Construction Sdn Bhd in 2002 as a Project Engineer and was involved in projects for process engineering design. In 2004, he left Toyo Engineering & Construction Sdn Bhd and joined WHESSOE Sdn Bhd as Project Engineer, where he was involved in various EPCC projects and hydropower plant projects. He was then promoted to Project Manager in 2011. </p>
										<p>In 2013, he left WHESSOE Sdn Bhd and joined OVE in the same year as a General Manager where he was tasked with overseeing operations, engineering and projects. </p>
										<p>He presently holds directorship and shareholdings in several private limited companies.</p>
									</div>
								</div>
							</div>
						</div>


						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/director-yau.png">
							</div>
							<div class="col-md-9">
								<h3>Yau Kah Tak</h3>
								<h6><span class="position">Executive Director</span></h6>
								<p>Yau Kah Tak, a Malaysian, aged 40, is our Executive Director. He was appointed to our Board on 1 April 2019. He joined our Group in 2014 and has been instrumental to the business growth and development of our Group since then. He is responsible for managing and implementing our Group’s business development plans and strategies.  </p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">

										<p>He obtained his Bachelor in Mechanical Engineering with honours from Universiti Teknologi Malaysia in 2001. He has 18 years of experience working in the EPC and project management for the O&G industry.</p>
										<p>In 2001, he joined Transocean Inc. under the Rig Engineer Trainee Programme, and was stationed in Brazil. The Rig Engineer Trainee Programme is a 3-year programme that has been designed to accelerate and advance personnel into management roles in the company. Upon completion of the programme in 2004, he assumed the role of Operations Engineer, where he worked closely with rig managers and offshore rig management teams in managing the day to day operations and rig maintenance activities.</p>
										<p>He was subsequently promoted to the position of Regional Operations Engineer – Performance in 2005, where he worked closely with the Director of Performance for Asia Pacific Unit on performance monitoring and improvement activities and operations lateral learning reviews. In 2006, he was promoted to Rig Manager – Asset and in the same year, became Rig Manager – Performance. In 2007, he joined Seadrill Management (S) Pte Ltd as an Operations Engineer where his role involved project management. </p>
										<p>Subsequently, in 2008, he joined Transocean Inc. as Rig Manager – Asset. In 2011, he was promoted to Strategic Sourcing Manager, where he was involved in pricing agreements, contracts management, vendor performance and management. He then joined Shelf Drilling Ventures Malaysia Sdn Bhd as a Rig Manager in 2013, where he was in charge of offshore rig management team and operational performance.</p>
										<p>In 2014, he left Shelf Drilling Ventures Malaysia Sdn Bhd and joined OVE in the same year, as a Business Development Manager. In August 2017, he left our Group to take a 1-year contract as a Rig Manager in charge of offshore rig management team and operational performance with Perisai Drilling Sdn Bhd. He rejoined OVE in September 2018.</p>
										<p>He presently holds directorship and shareholdings in several private limited companies.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/director-tham.png">
							</div>
							<div class="col-md-9">
								<h3>Tham Choi Kuen</h3>
								<h6><span class="position">Independent Non-Executive Director</span></h6>
								<p>Tham Choi Kuen, a Malaysian, aged 52, is our Independent Non-Executive Director. She was appointed to our Board on 1 April 2019 and is the Chairman of our Audit and Risk Management Committee. She is also a member of our Remuneration Committee and our Nomination Committee.</p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
										<p>In 2004, she obtained her professional accounting qualification from the Chartered Institute of Management Accountants (CIMA), United Kingdom. She has been a chartered accountant and a member of the Malaysian Institute of Accountants since 2005.</p>
                                        
<p>She began her career in 1997 as an Accounts Executive in the Accounts and Finance Department at Drard Holdings Sdn Bhd, where her duties included the preparation and maintenance of management reports, financial statements and related accounting reports. She was subsequently promoted to Assistant Manager in the Accounts and Finance department in 2000, where she was involved in management accounts reporting, budgeting, variance analysis, internal control, taxation, and financial and cash management.</p>

<p>In 2001, she left Drard Holdings Sdn Bhd to focus on her family and furthering her professional qualification in CIMA and subsequently re-joined the workforce in 2004 as Financial Controller at Azrahi Hotels Sdn Bhd (a subsidiary of Drard Holdings Sdn Bhd). During her tenure, she was responsible for overseeing the overall accounts, finance and treasury functions.</p>

<p>In 2007, she joined ITP Sdn Bhd as a Manager for student fees collections and accounts receivables management. She was then re-designated as Finance and Administrative Manager in 2009, where her duties included the preparation of the weekly sales and marketing cost reports for management review and ensuring proper quality control for all payment transactions. In 2017, she was promoted as the Senior General Manager of Finance, Credit Control and Administration of Kolej Universiti Linton Sdn Bhd, an affiliate of ITP Sdn Bhd where she was in-charge of the company’s financial and administrative department. </p>

<p>In June 2017, she left Kolej Universiti Linton Sdn Bhd and joined DWL Resources Berhad (formerly known as Spring Gallery Berhad) as a Financial Controller to oversee the corporate and financial aspects of the Company. In October 2017, she was promoted to Chief Financial Officer and is responsible for overseeing the overall finance-related functions of DWL Resources Berhad including monitoring of financial performance and results, financial reporting, treasury management and tax compliance.</P>

<p>She presently holds directorship in several private limited companies.</p>

										
									</div>
								</div>
							</div>
						</div>

						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/director-ilham.png">
							</div>
							<div class="col-md-9">
								<h3>Ilham Fadilah Binti Sunhaji</h3>
								<h6><span class="position">Independent Non-Executive Director</span></h6>
								<p>Ilham Fadilah Binti Sunhaji, a Malaysian, aged 37, is our Independent Non-Executive Director. She was appointed to our Board on 1 April 2019 and is the Chairman of our Nomination Committee. She is also a member of our Audit and Risk Management Committee and our Remuneration Committee. </p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
										<p>She completed her Bachelor of Arts majoring in International Studies (Global Security) and Political Science from the University of Wisconsin-Madison, United States of America in 2005. In 2014, she obtained her Masters of Business Administration in Strategic Management from the International Islamic University Malaysia. She is currently pursuing her Doctorate of Business Administration focusing on Corporate Governance in SEGI University.</p>
										<p>She began her career in Solid Partners Sdn Bhd in 2005 as an Executive in the Sales and Marketing department where she was involved in the tendering and procurement of military and medical equipment. In 2006, she joined Accenture Malaysia as an Analyst, where she executed change management work stream deliverables and facilitated the development of global technology solutions for the national automotive manufacturing company. </p>
										<p>In 2008, she was promoted as Consultant where she was involved in the establishment of global technology solutions division of a national oil and gas company. Subsequently, she was promoted to Manager in 2010, where she was involved in a few projects for the national O&G Company and national land public transportation. Her role included strategic planning, development and execution of organisation transformation plans, designing of high-level business operating models, as well as the development of deployment strategies and on-line knowledge repository systems.</p>
										<p>In 2012, she left Accenture Malaysia and joined Performance Management and Delivery Unit (“PEMANDU”), an agency of the Prime Minister’s Department as Manager. Her role included the rationalisation of subsidies and the facilitation of fuel standard upgrade. She was also involved in the human capital development, and was in charge of the implementation, assessment and facilitation of progress and development of two national key economic areas; oil, gas and energy as well as financial services under the Economic Transformation Programme.</p>
										<p>Subsequently, she was promoted to Senior Manager in 2014, where she was responsible to develop strategies and road maps for various industries. In March 2017, she was promoted to Vice President, where she facilitated discussion and negotiation on gas pricing as part of gas market liberalisation across ministries and agencies for the power and non-power sectors.</p>
										<p>In August 2017, she left PEMANDU and joined Pemandu Associates Sdn Bhd, a private management consultancy firm established by the preceding PEMANDU management, as Senior Vice President, where she is currently responsible for strategic planning and development and execution of organisation transformation plans.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="row teambg-left">
							<div class="col-md-3">
								<img src="images/director-reza.png">
							</div>
							<div class="col-md-9">
								<h3>Reza-Rizvy Bin Ahmad Rony Assim </h3>
								<h6><span class="position">Independent Non-Executive Director</span></h6>
								<p>Reza-Rizvy Bin Ahmad Rony Assim, a Malaysian, aged 39, is our Independent Non-Executive Director. He was appointed to our Board on 1 April 2019. He is member of our Audit and Risk Management Committee, Remuneration Committee and our Nomination Committee. </p>
								<div class="toggle">
									<div class="togglet">// Read More</div>
									<div class="togglec">
										<p>He completed his Matriculation Programme in Engineering from Universiti Putra Malaysia in 1999. He commenced his Bachelor in Engineering in Robotics and Mechatronics from the Swinburne University of Technology, Kuching, Sarawak in 2001 and graduated in 2009.</p>
										<p>While pursuing his degree on a part time basis from 2001 to 2009, from September 2006 to January 2009, he joined Naim Holdings Berhad as Sales Assistant in the Sales and Marketing department to help finance his studies. Subsequently, he took up industrial training with Sarawak Energy Berhad from January to July 2009.</p>
										<p>In 2010, he served as Private Secretary to the then Deputy Minister of Agriculture and Agro-based Industries. In the same year, he then served as Private Secretary to the then Deputy Minister of Domestic Trade, Cooperatives and Consumerism.</p>
										<p>Subsequently, in 2013, he served as Senior Private Secretary to the then Minister of Women, Family and Community Development up till 2018. During this period, he was also involved in Yayasan Kebajikan Negara, a non-profit organisation. As a board member he participated in the planning and execution of the organisation’s programmes and activities.</p>
										<p>In 2018, he joined Proviera SCD Biotech Sdn Bhd in Kuching, Sarawak, a company involved in probiotic technology for agriculture and industrial waste. As an Associate Marketing Director, he is responsible for developing and managing growth strategies for the company. He is also a member, since 2010, of Persatuan Anak Sarawak Semenanjung, a non-profit organisation that was established to help look into the welfare of Sarawakians in Peninsular Malaysia.</p>
										<p>He currently is a director and shareholder in several private limited companies.</p>
									</div>
								</div>
							</div>
						</div>


						<div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>


					</div>
				
				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->

@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop