@extends('layouts._partials.app')

@section('body')
<!-- Page Title
		============================================= -->
		<section id="page-title-mini" class="page-title-mini" style="background-image: url('images/bg-header.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class=" topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>Who Are We / Corporate Governance</h1> 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
							
		<section id="content">

			<div class="content-wrap nobottompadding">
					
				<div class="container">
					<div class="col_full">
						<div class="heading-block center">
							<h4>CORPORATE <span>GOVERNANCE</span></h4>
						</div>
					</div>

					<div class="cor-container">
						<div class="col-lg-12 center bottommargin-sm">
							<h5>The members of our Audit and Risk Management Committee as at LPD are as follows:</h5>
						</div>


						<div class="row">
                            <div class="col-lg-3 center cor-gov">
								<img src="images/cg-tham.png">
								<h5>Tham Choi Kuen</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Chairman</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-nor-azzam.png">
								<h5>Nor Azzam Bin Abdul Jalil</h5>
								<p class="center"><span class="position">Independent Non-Executive Chairman</span></p>
								<h5>Member</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-ilham.png">
								<h5>Ilham Fadilah Binti Sunhaji</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Member</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-reza.png">
								<h5>Reza-Rizvy Bin Ahmad Rony Assim</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Member</h5>
							</div>
						</div>
					</div>

					<div class="cor-container">
						<div class="col-lg-12 center bottommargin-sm">
							<h5>The members of our Remuneration Committee as at LPD are as follows:</h5>
						</div>

						<div class="row">
                            <div class="col-lg-3 center cor-gov">
								<img src="images/cg-nor-azzam.png">
								<h5>Nor Azzam Bin Abdul Jalil</h5>
								<p class="center"><span class="position">Independent Non-Executive Chairman</span></p>
								<h5>Chairman</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-ilham.png">
								<h5>Ilham Fadilah Binti Sunhaji</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Member</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-tham.png">
								<h5>Tham Choi Kuen</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Member</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-reza.png">
								<h5>Reza-Rizvy Bin Ahmad Rony Assim</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Member</h5>
							</div>
						</div>
					</div>

					<div class="cor-container" style="margin-bottom: 150px">

						<div class="col-lg-12 center bottommargin-sm">
						<h5>The members of our Nominating Committee as at LPD are as follows:</h5>
						</div>

						<div class="row">
                            <div class="col-lg-3 center cor-gov">
								<img src="images/cg-ilham.png">
								<h5>Ilham Fadilah Binti Sunhaji</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Chairman</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-nor-azzam.png">
								<h5>Nor Azzam Bin Abdul Jalil</h5>
								<p class="center"><span class="position">Independent Non-Executive Chairman</span></p>
								<h5>Member</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-tham.png">
								<h5>Tham Choi Kuen</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Member</h5>
							</div>
							<div class="col-lg-3 center cor-gov">
								<img src="images/cg-reza.png">
								<h5>Reza-Rizvy Bin Ahmad Rony Assim</h5>
								<p class="center"><span class="position">Independent Non-Executive Director</span></p>
								<h5>Member</h5>
							</div>
						</div>

					</div>
				</div> 
				<div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				  </div>

			</div>

		</section><!-- #content end -->
@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
