@extends('layouts._partials.app')

@section('body')
<!-- Page Title
		============================================= -->
		<section class="page-title-mini" style="background-image: url('images/bg-header-whatwedo.jpg'); background-position: center center;">
			<div class="container clearfix">
				<img src="images/header-title.png" class="pull-right topmargin-lg">
			</div>
		</section>
		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>What We Do / Upstream</h1> 
				
			</div>
		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->					
		<section id="content">
			<div class="content-wrap nobottompadding">
					
				<div class="container">
					<div class="col_full">
							<div class="heading-block center">
								<h3>BUSINESS OVERVIEW <span>UPSTREAM</span></h3>
								 <span>our products &amp; Services for downstream oil &amp; gas</span>
							</div>	
					</div>
				</div><!--end container-->
				
				<div class="section dark ovhbgimg nomargin nopadding">
                    
					<div class="container">
					     <div class="col_half nopadding nobottommargin xs-toppadding">							 
							<div class="portfolio-image">
								<div class="fslider" data-arrows="false" data-speed="650" data-pause="3500" data-animation="fade">
									<div class="flexslider">
										<div class="slider-wrap">
											<div class="slide"><img src="images/client-pic1.jpg"></div>
											<div class="slide"><img src="images/client-pic2.jpg"></div>
										</div>
									</div>
								</div>
							</div>
						 </div>
						 <div class="col_half col_last nopadding nobottommargin">
						
					    	<div class="heading-block topmargin-lg bottommargin-sm">
								<h1 class="text-exlarge">HOW </h1>
								<h3 class="text-white text-exlarge2">WE DO IT</h3>
							</div>
							 <p>
							    Through resourceful partnership,
								a proactive customer-centric
								focus, local presence in ports & quaysides,
								and unyielding dedication to delivering the 
								highest quality service. 
							 </p> 
							 
						</div>
					 
					</div>
				</div>
				
                
                <div class="section notopmargin" style="background-image: url('images/bg-upstream.jpg'); background-position: center center;">

					<div class="container clearfix">

						
						<div class="row">
                            
                         <div class="col-lg-4 col-md-4 bottommargin">
                             
                                 <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/rig-modification.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Rig/Platform Modifications</h2>
											</div>
										</div>
									</div><!-- 
                             
                              <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/rig-activation.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Rig Activation</h2>
											</div>
										</div>
									</div> -->
                             
                             
                               <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/rig-stacking.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Rig Stacking & Reactivation</h2>
											</div>
										</div>
									</div>
                             
                             <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/rig-repair.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Rig Repair & Maintenance</h2>
											</div>
										</div>
									</div>
                             
                              <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/accommodation.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Accommodation Refurbishment</h2>
											</div>
										</div>
									</div>

									

									<div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/fabrication-construction.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Fabrication & Construction of Structures</h2>
											</div>
										</div>
									</div>
                           
                           
                        
						</div>
                            
                        <div class="col-lg-4 col-md-4 bottommargin">	
                            
                            <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/blasting-painting.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Blasting & Painting Services</h2>
											</div>
										</div>
									</div>
                            
                             <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/equipment-rental.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Equipment Rental, Repair
                                                <br>& Maintenance</h2>
											</div>
										</div>
									</div>
                            
                              <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/marine-security.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Marine Security Services</h2>
											</div>
										</div>
									</div>
                            
                             <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/ndt-inspection.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Conventional NDT & Inspection</h2>
											</div>
										</div>
									</div>

                        

						</div>
                         <div class="col-lg-4 col-md-4 col_last bottommargin">
                             
                             <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/rope-access.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Rope Access</h2>
											</div>
										</div>
									</div>
                             
                             <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/scaffolding.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Scaffolding Services</h2>
											</div>
										</div>
									</div>
                             
                             <div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/cleaning-repair.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Tank Cleaning</h2>
											</div>
										</div>
									</div>


									<div class="spost clearfix">
										<div class="entry-image">
											<img src="images/upstream/cranerepair.jpg" alt="">
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h2>Crane, Structural, Piping Replacement</h2>
											</div>
										</div>
									</div>

                             
						</div>
                        <!--end col last-->

                            
						</div><!--end row-->

					</div>

				</div>
				
				
				 <div class="promo promo-dark promo-flat promo-full  header-stick bg-dark bg-ovgroup">
						<div class="container clearfix">
							<h3>Call us today at <span>+603-5886 2555</span> or Email us at <span>info@ovbhd.com</span></h3>
							<span>We strive to provide Our Customer with Top Notch Support to make thier Business Experience Wonderful</span>
							<a href="tel:+60358862555" class="button button-large button-rounded button-border button-light"><i class="icon-phone1"></i><span>Call Us Now</span></a>
						</div>
				 </div>
				
			</div>			
		</section><!-- #content end -->
		
		
@stop

@section('modal')
@stop


@section('page_style')
@stop

@section('page_script')
@stop

@section('init_script')
@stop
